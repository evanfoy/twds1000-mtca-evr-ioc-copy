Generic EVR implementation for ESS.
--

# Introduction
The implementation is prepared to cover ESS requirements for all subsystems. The content is defined but not closed and based on many ESS documents and meetings. The EVR is planned to be continuously updated with new features according to new requirements. All the contributions are welcome.

# User space
* [Contributions](CONTRIBUTING.md)
* [New feature requests](https://jira.esss.lu.se/browse/ICSHWI-4513)

# MTCA backplane triggers

| Trig | Signal | Function | Dir | Generic | Full        | FBIS       | Struck (LLRF) |
| ---- | ------ | -------- | --- | ------- | ----------  | ---------- | ------------- |
| NA   | TCLKA  | RF CLK   | OUT | RFl/4   | RFl/4       | RFl/4      | RFl/4         |
| NA   | TCLKB  | RF CLK   | OUT | -       | -           | -          | -             |
| 0    | RX17   | Trigger  | OUT | 14Hz    | 14Hz        | 14Hz       | 14Hz          |
| 1    | TX17   | Trigger  | OUT | RFSt    | RFSt        | RFSt       | RFSt          |
| 2    | RX18   | Trigger  | OUT | BPulse  | BPulse      | BPulse     | BPulse        |
| 3    | TX18   | Trigger  | OUT | PMortem | PMortem     | PMortem    | PMortem       |
| 4    | RX19   | Trigger  | IN  | -       | PMortemSys  | PMortemSys | -             |
| 5    | TX19   | Trigger  | -   | -       | -           | -          | NA (blocked)  |
| 6    | RX20   | Trigger  | OUT | DoD     | DoD         | -          | NA (blocked)  |
| 7    | TX20   | Trigger  | IN  | DoDSys  | DoDSys      | -          | -             |

* [Events/Triggers Reference](https://confluence.esss.lu.se/display/HAR/Event+and+Trigger+Reference)
* [EVR PVs](https://gitlab.esss.lu.se/e3/ts/e3-mrfioc2/-/raw/master/docs/ESS-EVR-v2.3.txt)